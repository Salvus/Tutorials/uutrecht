from pathlib import Path
from typing import List

import matplotlib.pyplot as plt
import numpy as np
import obspy
from mpl_toolkits.axes_grid1 import make_axes_locatable
from salvus_mesh import StructuredGrid2D, unstructured_mesh
from scipy.interpolate import RectBivariateSpline, UnivariateSpline
from scipy.ndimage import gaussian_filter

from collections import namedtuple

Header2D = namedtuple("Header2D", ["x0", "y0", "dx", "dy", "nx", "ny"])


def read_segy(file_name: Path, header: Header2D):
    """
    Read a segy file into a numpy array.
    :param file_name: SEGY file name.
    :param header: Model header.
    :return: Parameter as 2-D array.
    """
    n0, n1 = header.nx, header.ny
    assert file_name.exists(), file_name
    parameter = np.empty((n0, n1))
    st = obspy.read(str(file_name))
    for _i, tr in enumerate(st):
        parameter[_i, :] = tr.data

    return parameter


def rho_from_gardeners(vp: np.ndarray):
    """
    Compute density using Gardner's relationship.
    :param vp: VP array.
    """
    return 310 * vp ** 0.25


def vs_from_poisson(vp: np.ndarray, poisson_ratio=0.25):
    """
    Compute VS from VP given a certain Poisson's ratio.
    :param vp: VP array.
    :param poisson_ratio: Poisson's ratio to apply.
    """
    return np.sqrt((1 - 2 * poisson_ratio) / (2 * (1 - poisson_ratio))) * vp


def find_first_discontinuity(parameter: np.ndarray, header: Header2D):
    """
    Find a discontinuity in a SEG-Y file. Heuristically, for best matching with synthetics generated with FD, I've
    found it best to put a discontinuity between two FD grid points.
    :param parameter: Parameter to use for discontinuity proxy.
    :param header: Model header.
    :return: Discontinuity depths (in m) and the index where the discontinuity begins in the SEGY file.
    """
    dx, dy = header.dx, header.dy
    layers = np.nonzero(np.abs(np.gradient(parameter, axis=0)).transpose() > 1)
    indices = np.array(layers[1][np.append([0], np.where(layers[0][:-1] != layers[0][1:])[0] + 1)], dtype=int)
    depths = -dy * (indices + 1 / 2) + header.y0
    return UnivariateSpline(np.arange(header.nx) * dx + header.x0, depths, s=10000000, ext='const'), indices


def plot_parameters(parameter: List[np.ndarray], names: List[str], header: Header2D):
    """
    Plot the SEG-Y file.
    :param parameter: Parameter to plot.
    :param names: Parameter names (for plot titles).
    :param header: Model header.
    :return: Figure and plot axes.
    """

    x0, y0 = header.x0, header.y0
    dx, dy = header.dx, header.dy
    f, ax = plt.subplots(len(parameter), 1, figsize=(15, 5 * len(parameter)), squeeze=False, sharex=True)
    for _i, p in enumerate(parameter):
        a = ax[_i, 0]
        extent = (x0, dx * p.shape[1] + x0, -dy * p.shape[0] + y0, y0)
        image = a.imshow(p, extent=extent)
        a.set_title(names[_i])

        divider = make_axes_locatable(a)
        cax = divider.append_axes("right", size="2%", pad=0.05)
        f.colorbar(image, cax=cax)

        a.set_ylabel("Depth (m)")

    ax.flatten()[-1].set_xlabel("Distance (m)")
    return f, ax


def get_element_size(v_min: float, frequency: float, n_elem_per_wavelength=2):
    """
    Get the appropriate edge size for an element.
    :param v_min: Minimum velocity.
    :param frequency: Frequency.
    :param n_elem_per_wavelength: Elements per wavelength.
    :return: Appropriate element edge length.
    """
    return v_min / frequency / n_elem_per_wavelength


def deform(mesh: unstructured_mesh.UnstructuredMesh, surface: UnivariateSpline, header: Header2D) -> unstructured_mesh:
    """
    Deform the mesh along a surface.
    :param surface: Line along which to deform.
    :param header: Model header.
    :return: Deformed mesh.
    """

    dx = header.dx
    x = np.arange(header.nx * dx + header.x0)
    surface_interp = surface(x)

    # Find ocean bottom before deformation and make it a side set.
    fluid = np.require(mesh.elemental_fields["fluid"], dtype=np.bool)
    ocean_height = mesh.points[mesh.connectivity[fluid]][:, :, 1].min()

    mesh.find_side_sets()
    mesh.find_side_sets_generic(
        "ocean_bottom",
        distance=lambda x: np.abs(x[:, 1] - ocean_height))

    mesh.tensorize(order=1)
    mesh.add_dem_2D(
        x, surface_interp - np.min(surface_interp),
        y0=np.min(mesh.points[:, 1]),
        y1=np.max(mesh.points[:, 1]),
        yref=np.min(surface_interp),
        kx=1, ky=1)

    mesh.apply_dem()
    return mesh


def __get_mesh_chunk(max_element_size: float, nx: int, dx: float,
                     min_x: float, max_x: float, min_y: float,
                     max_y: float, n_elem_y: np.ndarray):
    """
    :param max_element_size: Max edge length.
    :param nx: Number of SEGY grid points in x.
    :param dx: SEGY dx.
    :param min_x: Minimum x of mesh.
    :param max_x: Maximum x of mesh.
    :param min_y: Minimum y of mesh.
    :param max_y: Maximum y of mesh.
    :param n_elem_y: Number of elements in the y-direction.
    :return: Structured mesh chunk.
    """
    n_elem_x = np.ceil((nx - 1) * dx / max_element_size).astype(int)
    return StructuredGrid2D.rectangle_vertical_refine(
        nelem_x=n_elem_x, nelem_y=n_elem_y, min_x=min_x,
        max_x=max_x, min_y=min_y, max_y=max_y)


def generate_mesh_chunks(minimum_velocity: np.ndarray, maximum_frequency: float,
                         header: Header2D, depths: UnivariateSpline,
                         extrude: float = 0.0, extrude_top: float=0.0) \
        -> unstructured_mesh:
    """
    Generate the mesh skeleton.
    :param minimum_velocity: Spatially variable minimum velocity.
    :param maximum_frequency: Maximum frequency to simulate.
    :param header: Model header.
    :param extrude: Extrude the mesh along x0, x1, and y0 directions.
    :return Mesh skeleton.
    """

    nx, ny = header.nx, header.ny
    dx, dy = header.dx, header.dy
    x0, y0 = header.x0, header.y0
    y1 = y0 + ny * -dy

    max_element_size = get_element_size(np.min(minimum_velocity), maximum_frequency)
    n_elem_x = np.ceil((nx - 1) * dx / max_element_size).astype(int)
    x_grid = np.linspace(x0, (n_elem_x + 1) * max_element_size, n_elem_x + 1)
    n_elem_y_m1 = np.ceil((y0 - depths(x_grid)) / max_element_size).astype('int')
    n_elem_y_m2 = np.ceil((depths(x_grid) - y1) / max_element_size).astype('int')

    chunk0 = __get_mesh_chunk(max_element_size, nx, dx,
                              min_x=x0 - extrude, max_x=x0 + dx * nx + extrude,
                              min_y=np.min(depths(x_grid)), max_y=y0 + extrude_top,
                              n_elem_y=n_elem_y_m1)
    chunk1 = __get_mesh_chunk(max_element_size, nx, dx,
                              min_x=x0 - extrude, max_x=x0 + dx * nx + extrude,
                              min_y=-dy * ny - extrude + y0, max_y=np.min(depths(x_grid)),
                              n_elem_y=n_elem_y_m2)

    chunk0_um = chunk0.get_unstructured_mesh()
    chunk1_um = chunk1.get_unstructured_mesh()
    chunk0_um.attach_field("fluid", np.ones(chunk0_um.nelem))
    chunk1_um.attach_field("fluid", np.zeros(chunk1_um.nelem))

    return chunk0_um + chunk1_um


def attach_parameters(mesh: unstructured_mesh, names: List[str], parameters: List[np.ndarray],
                      header: Header2D, smoothing_std: float, ocean_bottom_depths=None,
                      ocean_bottom_indices=None, remove_ocean=False, version="V1"):
    """
    Attach a set of (smoothed) parameters to a mesh.
    :param mesh: Mesh to attach parameters on.
    :param names: Names of parameters.
    :param parameters: Values of parameters.
    :param header: Model header.
    :param smoothing_std: Standard deviation in m of the smoothing filter.
    :param ocean_bottom_depth: An array defining the depth to the ocean bottom.
    :param ocean_bottom_indices: An array defining the ocean bottom indices from the segy file.
    :param remove_ocean: Remove the ocean (create a free surface).
    :param version: Generate a Salvus version 1 or version 2 mesh.
    """

    x0, y0 = header.x0, header.y0
    nx, ny = header.nx, header.ny
    dx, dy = header.dx, header.dy

    x = np.arange(nx) * dx + x0
    y = np.arange(ny)[::-1] * -dy + y0

    if version == "V2":
        pass
        #  mesh.tensorize(order=2)

    # Ocean or not.
    c = mesh.get_element_centroid()
    for n, p in zip(names, parameters):

        # Exclude oceanic velocities from smoothing.
        if ocean_bottom_indices is not None:
            for i in np.arange(p.shape[1]):
                p[:ocean_bottom_indices[i], i] = p[ocean_bottom_indices[i] + 1, i]

        # Interpolate SEGY velocities.
        interpolator = RectBivariateSpline(x, y, gaussian_filter(p, smoothing_std / dx).transpose()[:, ::-1])
        for _i in range(mesh.nodes_per_element):
            mesh.attach_field(
                f"{n}_{_i}", interpolator(*mesh.points[mesh.connectivity][:, _i].transpose(), grid=False))

            # Reinterpolate ocean velocities.
            if (mesh.elemental_fields["fluid"] == 0).any():
                mesh.elemental_fields[f"{n}_{_i}"][mesh.elemental_fields["fluid"] == 1] = p[0, 0]

    # Optionally remove ocean.
    if ocean_bottom_depths is not None:
        if remove_ocean:
            mesh = mesh.apply_element_mask(c[:, 1] < ocean_bottom_depths(c[:, 0]))

    return mesh
