{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style='background-image: url(\"header.png\") ; padding: 0px ; background-size: cover ; border-radius: 5px ; height: 250px'>\n",
    "    <div style=\"float: right ; margin: 50px ; padding: 20px ; background: rgba(255 , 255 , 255 , 0.7) ; width: 50% ; height: 150px\">\n",
    "        <div style=\"position: relative ; top: 50% ; transform: translatey(-50%)\">\n",
    "            <div style=\"font-size: xx-large ; font-weight: 900 ; color: rgba(0 , 0 , 0 , 0.8) ; line-height: 100%\">Salvus</div>\n",
    "            <div style=\"font-size: large ; padding-top: 20px ; color: rgba(0 , 0 , 0 , 0.5)\">Kernels in Anisotropic Media</div>\n",
    "        </div>\n",
    "    </div>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The nature of the spectral-element method allows us to accurately simulate wave propagation in environments with significant topography. This feature is very useful when considering land-seismic surveys. In this short tutorial we'll use the classic Foothills model as a background model within which to compute anisotropic kernels for full-waveform inversion. We'll read in the SEG-Y file defining the model, mesh it up automatically, and then generate an energy kernel for receivers placed down a borehole. As always, we'll start by loading our Python libraries."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Python libraries.\n",
    "import os\n",
    "import copy\n",
    "import toml\n",
    "import h5py\n",
    "import shutil\n",
    "import numpy as np\n",
    "\n",
    "from pathlib import Path\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    " # Salvus.\n",
    "import pyasdf            # ASDF library for visualizing seismograms.\n",
    "import salvus_mesh       # Salvus meshing toolbox.\n",
    "import salvus_flow.api   # Salvus workflow toolbox.\n",
    "\n",
    "# Shortcuts\n",
    "from salvus_mesh.skeleton import Skeleton\n",
    "from salvus_flow.workflow import Workflow\n",
    "from salvus_mesh.unstructured_mesh import UnstructuredMesh\n",
    "from salvus_flow.workflow.helpers.adjoint_source_computation \\\n",
    "    import get_simple_compute_adjoint_sources_callback\n",
    "\n",
    "# Helper routines for meshing SEG-Y files.\n",
    "import segy_mesh_2d"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setup\n",
    "\n",
    "In this tutorial we will build on what we learned in the imaging tutorial, and compute gradients in anisotropic media using two different parameterizations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "SALVUS_FLOW_SITE = \"local\"\n",
    "MODEL_PATH = \"foothills.h5\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next we need to set some basic parameters which will govern our simulations. Since this is a meant for a smaller-scale tutorial, we'll keep things computationally inexpensive by limiting the size of the mesh, the number of sources, and the frequencies which we'll model. Note that numerical wavefield extrapolations in 2-D usually scale with frequency$^3$, so it is this value which is most important when it comes to determining the computational cost."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Parameters governing our simulations.\n",
    "NUM_SOURCES = 1\n",
    "NUM_RECEIVERS = 1000\n",
    "SOURCE_CENTER_FREQUENCY = 1\n",
    "\n",
    "# Parameters governing mesh size. \n",
    "MESH_FREQUENCY = 2 # Hz.\n",
    "TIME_STEP = 1e-3   # Important for correct adjoint source."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Meshing SEG-Y files\n",
    "\n",
    "To aid Salvus_Mesh_ in the meshing process, we'll explicitly specify the information contained in the SEG-Y headers, and create our own lightweight header to pass the the SEG-Y meshing routines."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dx, dy = 15, 10\n",
    "nx, ny = 1668, 1000\n",
    "x0, y0 = 0.0, 2000.0\n",
    "header = segy_mesh_2d.Header2D(x0=x0, y0=y0, nx=nx, ny=ny, dx=dx, dy=dy)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we'll read in the regular-gridded VP SEG-Y file, and compute some proxies for the other elastic parameters we are interested in. In this case we'll use a default Poisson's ratio of $0.25$, and use Gardner's relationship to compute density from VP."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "vp = segy_mesh_2d.read_segy(Path(\"./data/velocity.segy.gz\"), header).transpose()\n",
    "vs = segy_mesh_2d.vs_from_poisson(vp)\n",
    "rho = segy_mesh_2d.rho_from_gardeners(vp)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can call the SEG-Y mesh helper to generate our mesh. This will happen in 3 stages. First, we'll "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_anisotropic_parameters(vp: np.ndarray, vs: np.ndarray, \n",
    "                               rho: np.ndarray, percent_perturb: float=0.1):\n",
    "    \n",
    "    vph = vp * (1 + percent_perturb)\n",
    "    vpv = vp\n",
    "    \n",
    "    c11 = rho * vph * vph\n",
    "    c22 = rho * vpv * vpv\n",
    "    c33 = rho * vs * vs\n",
    "    c12 = c22 - 2 * c33\n",
    "    c13 = np.zeros_like(c11)\n",
    "    c23 = np.zeros_like(c11)\n",
    "    \n",
    "    return [\"C11\", \"C12\", \"C13\", \"C22\", \"C23\", \"C33\", \"RHO\"], [c11, c12, c13, c22, c23, c33, rho]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Find the first discontinuity in depth.\n",
    "depths, indices = segy_mesh_2d.find_first_discontinuity(vp, header)\n",
    "\n",
    "# Generate the mesh and interpret the computed depths as the ocean bottom.\n",
    "mesh = segy_mesh_2d.generate_mesh_chunks(vp, MESH_FREQUENCY, header, depths=depths)\n",
    "\n",
    "# Deform the mesh to respect the ocean bottom.\n",
    "mesh = segy_mesh_2d.deform(mesh, depths, header)\n",
    "\n",
    "# Get anisotropic parameterization.\n",
    "names, vals = get_anisotropic_parameters(vp, vs, rho)\n",
    "\n",
    "# Attach the elastic parameters to the mesh.\n",
    "mesh = segy_mesh_2d.attach_parameters(mesh, names, vals, header, \n",
    "                                      smoothing_std=400, version=\"V2\", ocean_bottom_depths=depths,\n",
    "                                      ocean_bottom_indices=indices, remove_ocean=True)\n",
    "\n",
    "# Write the mesh and visualize.\n",
    "mesh.write_h5_tensorized_model(MODEL_PATH);mesh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Sources and receivers\n",
    "\n",
    "As in the previous tutorial, we'll now set up our sources and receivers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Source and receiver locations.\n",
    "rec_x = 4000\n",
    "src_x = [23000.0]\n",
    "\n",
    "src_y = 500.0\n",
    "rec_y = np.linspace(500.0, -7000.0, 100)\n",
    "\n",
    "# Setup receivers.\n",
    "receivers = [{\n",
    "    \"network-code\": \"XX\",\n",
    "    \"station-code\": f\"{_i:03d}\",\n",
    "    \"medium\": \"solid\",\n",
    "    \"location\": [float(rec_x), float(y)]}\n",
    "    for _i, y in enumerate(rec_y)]\n",
    "\n",
    "# Generate simulation objects.\n",
    "simulations = {\n",
    "    f\"simulation_{i:03d}\": {\n",
    "        \"source\": [{\n",
    "            \"location\": (x, src_y),\n",
    "            \"spatial-type\": \"vector\",\n",
    "            \"spatial-weights\": [0.0, 1e9],\n",
    "            \"source-time-function\": {\n",
    "                \"type\": \"ricker\",\n",
    "                \"center-frequency\": SOURCE_CENTER_FREQUENCY\n",
    "            }\n",
    "        }],\n",
    "        \"receiver\": copy.deepcopy(receivers)        \n",
    "    } for i, x in enumerate(src_x) \n",
    "}\n",
    "\n",
    "# Attach to mesh to get a quick visualization.\n",
    "mesh._meta = {\"simulations\": simulations}\n",
    "mesh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The rest of this tutorial is also rather familiar. Instead of an acoustic source, we instead define an elastic vector source to get our wavefield moving. You could switch this to a `moment-tensor` source if you are so inclined."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_master_input(mesh_filename: str, simulations: dict, movie=False):\n",
    "    \"\"\"\n",
    "    Generate a skelton input file which can be passed to SalvusFlow.\n",
    "    \n",
    "    :param mesh_filename: Mesh to use in the simulation.\n",
    "    :param simulations: Dictionary of simulations (one entry per shot).\n",
    "    :param movie: Generate a movie of the wavefield.\n",
    "    \"\"\"\n",
    "\n",
    "\n",
    "    # On which hardware will we run?\n",
    "    master_file = {\n",
    "        \"hardware\": {\"gpu\": False}\n",
    "    }\n",
    "    \n",
    "    # Provide information on the specific mesh and the model.\n",
    "    master_file.update({\"domain\": {\n",
    "            \"dimension\": 2,\n",
    "            \"polynomial-order\": 4,\n",
    "            \"mesh\": {\n",
    "                \"filename\": mesh_filename,\n",
    "                \"format\": \"hdf5\"\n",
    "            },\n",
    "            \"model\": {\n",
    "                \"filename\": mesh_filename,\n",
    "                \"format\": \"hdf5\"\n",
    "            }            \n",
    "        }})\n",
    "    \n",
    "    # Provide information on the physical equation.\n",
    "    master_file.update({       \n",
    "        \"physics\": {\n",
    "            \"wave-equation\": {\n",
    "                \"time-stepping-scheme\": \"newmark\",\n",
    "                \"start-time-in-seconds\": -0.7,\n",
    "                \"end-time-in-seconds\": 7.0,\n",
    "                \"time-step-in-seconds\": TIME_STEP,\n",
    "                \"attenuation\": False,\n",
    "                \"point-source\": simulations[\"source\"],\n",
    "                \"boundaries\": [\n",
    "                    {\n",
    "                        \"type\": \"absorbing\",\n",
    "                        \"side-sets\": [\"x0\", \"x1\", \"y0\"],\n",
    "                        \"taper-amplitude\": 0.0,\n",
    "                        \"width-in-meters\": 0.0\n",
    "                    }\n",
    "                ]\n",
    "            }\n",
    "        }})\n",
    "    \n",
    "    # Provide information relating to point receivers.\n",
    "    master_file.update({\n",
    "        \"output\": {\n",
    "            \"point-data\": {\n",
    "                \"fields\": [\"displacement\"],\n",
    "                \"sampling-interval-in-time-steps\": 1,\n",
    "                \"filename\": \"receivers.h5\",\n",
    "                \"format\": \"asdf\",\n",
    "                \"receiver\": simulations[\"receiver\"]\n",
    "            }\n",
    "        }})\n",
    "    \n",
    "    # Generate a movie if we'd like.\n",
    "    if movie:\n",
    "        master_file[\"output\"].update({\n",
    "            \"volume-data\": {\n",
    "                \"fields\": [\"displacement\"],\n",
    "                \"sampling-interval-in-time-steps\": 100,\n",
    "                \"filename\": \"movie.h5\",\n",
    "                \"format\": \"hdf5\",\n",
    "                \"polynomial-order\": 4\n",
    "            }\n",
    "        })\n",
    "    \n",
    "    return master_file"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, as before, let's run a forward simulation and check out the wavefield movie."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data_folder = Path(\"./observed_data\")\n",
    "for name, value in simulations.items():\n",
    "    \n",
    "    # Generate the input file.\n",
    "    input_file = get_master_input(\n",
    "        mesh_filename=MODEL_PATH,\n",
    "        simulations=value, movie=True)\n",
    "    \n",
    "    # Run salvus.\n",
    "    salvus_flow.api.run(\n",
    "        site_name=SALVUS_FLOW_SITE, input_file=input_file, \n",
    "        output_folder=data_folder / name, \n",
    "        get_all=True, overwrite=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Generating kernels\n",
    "\n",
    "As outlined in the imaging tutorial, the next several cells serve to set up Salvus_Flow_ for kernel generation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_meta_config(ranks=4):\n",
    "    \"\"\"\n",
    "    Return the 'meta' group for our RTM workflow.\n",
    "    :param ranks: Number of ranks to run the wave equation with.\n",
    "    \"\"\"\n",
    "    return {\n",
    "        \"ranks_per_simulation\": ranks,\n",
    "        \"submit_all_simulations_at_once\": False,\n",
    "        \"walltime_forward_waveform_simulation_in_seconds\": 60,\n",
    "        \"walltime_adjoint_waveform_simulation_in_seconds\": 60,\n",
    "        \"max_number_of_retries\": 1\n",
    "    }"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We also need to specify a minimal set of parameters which describe the forward and adjoint wave simulations, which Salvus_Flow_ will pass on the Salvus_Compute_. The sources and receivers themselves do not need to be specified here -- we'll handle these later on."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_wave_propagation_config():\n",
    "    \"\"\"\n",
    "    Return the 'wave_propagation_settings' group for our FWI workflow.\n",
    "    \"\"\"\n",
    "    return {\n",
    "        \"physics\": {\n",
    "            \"wave-equation\": {\n",
    "                \"start-time-in-seconds\": -0.7,\n",
    "                \"end-time-in-seconds\": 7.0,\n",
    "                \"time-step-in-seconds\": TIME_STEP,\n",
    "                \"boundaries\": [\n",
    "                    {\n",
    "                        \"type\": \"absorbing\",\n",
    "                        \"side-sets\": [\"x0\", \"x1\", \"y0\"],\n",
    "                        \"taper-amplitude\": 0.0,\n",
    "                        \"width-in-meters\": 0.0\n",
    "                    }\n",
    "                ]\n",
    "            }\n",
    "        },\n",
    "        \"domain\": {\n",
    "            \"dimension\": 2,\n",
    "            \"polynomial-order\": 4,\n",
    "        },\n",
    "         \"output\": {\n",
    "            \"point-data\": {\n",
    "                \"fields\": [\"displacement\"],\n",
    "                \"sampling-interval-in-time-steps\": 1\n",
    "            },\n",
    "            \"volume-data\": {\n",
    "                \"sampling-interval-in-time-steps\": 1,\n",
    "                \"polynomial-order\": 4\n",
    "            }\n",
    "        }\n",
    "    }"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we give Salvus some details regarding the gradients we want to create. In order to bypass the process of creating a separate set of \"true data\", we'll use an energy norm to compute an adjoint source directly from the synthetic data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_sources_and_receivers():\n",
    "    \"\"\"\n",
    "    SalvusFlow simply expects our \"simulations\" dictionary to determine\n",
    "    the source and receiver paths.\n",
    "    \"\"\"\n",
    "    return simulations\n",
    "\n",
    "def adjoint_source_function(synthetic_tr, observed_tr):\n",
    "    \"\"\"\n",
    "    This function will be called for every trace to compute\n",
    "    the actual adjoint source.\n",
    "    \n",
    "    It takes two ObsPy Trace objects (so meta-data like\n",
    "    sampling rates and so on is still present) but expects\n",
    "    a numpy array to be returned.\n",
    "    \n",
    "    :param synthetic_tr: Synthetic obspy trace.\n",
    "    :param observed_tr: \"Observed\" obspy trace.\n",
    "    \"\"\"\n",
    "    return -TIME_STEP * synthetic_tr.data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Again, we need to inform _Flow_ of where to find the relevant files."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Where is our observed data stored?\n",
    "observed_data_map = {\n",
    "    name: data_folder / name / \"receivers.h5\"\n",
    "    for name in simulations.keys()\n",
    "}\n",
    "\n",
    "# Wrap the adjoint source function in a language SalvusFlow will understand.\n",
    "compute_adjoint_sources = get_simple_compute_adjoint_sources_callback(\n",
    "    observed_data_map=observed_data_map,\n",
    "    adjoint_source_function=adjoint_source_function,\n",
    "    ndim=2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And again, we need to set up a small callback function which allows us to specify which gradients we want to compute."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_gradient_settings(parameterization: str=\"linear\"):\n",
    "    \"\"\"\n",
    "    Return the \"gradient\" group for use with salvus flow. \n",
    "    Change the parameterization here for different imaging conditions.\n",
    "    \n",
    "    :param parameterization: Compute the material gradients with respect to this parameterization.\n",
    "    \"\"\"\n",
    "    return {\n",
    "        \"initial_model\": MODEL_PATH,\n",
    "        \"parameterization\": parameterization\n",
    "    }"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we're all ready to go! The last step is to consolidate everything into a simple structure which can be passed to Salvus flow. We'll do this below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Construct our master input file.\n",
    "workflow_config = {\n",
    "    \"meta\": get_meta_config(),\n",
    "    \"material_gradients\": get_gradient_settings(),\n",
    "    \"wave_propagation_settings\": get_wave_propagation_config(),\n",
    "    \"callbacks\": {\n",
    "        \"get_sources_and_receivers\": get_sources_and_receivers,\n",
    "        \"compute_adjoint_sources\": compute_adjoint_sources\n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "...and now we're ready! Below we'll run the linear \"ComputeMaterialGradients\" workflow, which will run the required forward and adjoint simluations required to compute a gradient. Everything, including all relevant intermediate files, will be stored in the \"workflow_run_directory\". The final result (in this case, the image) will then be copied into another Salvus_Mesh_ file for us to visualize and interpret."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Remove the output directory if it already exists.\n",
    "if os.path.exists(\"workflow_run_directory\"):\n",
    "    shutil.rmtree(\"workflow_run_directory\")\n",
    "\n",
    "# Run our FWI workflow!!\n",
    "w = Workflow(base_dir=\"workflow_run_directory\",\n",
    "             workflow_type=\"ComputeMaterialGradients\",\n",
    "             salvus_flow_site_name=SALVUS_FLOW_SITE,\n",
    "             config=workflow_config,\n",
    "             use_celery=False)\n",
    "\n",
    "w.run()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gradient = UnstructuredMesh.from_h5(\"workflow_run_directory/gradient.h5\")\n",
    "gradient"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exploring alternate parameterizations\n",
    "\n",
    "As in the imaging example, we can switch the form of the gradients we compute to something more familiar, for instance the Love parameters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "workflow_config[\"material_gradients\"].update(\n",
    "    get_gradient_settings(\"love-parameters\")\n",
    ")\n",
    "\n",
    "# Remove the output directory if it already exists.\n",
    "if os.path.exists(\"workflow_run_directory_love\"):\n",
    "    shutil.rmtree(\"workflow_run_directory_love\")\n",
    "\n",
    "# Run our FWI workflow!!\n",
    "w = Workflow(base_dir=\"workflow_run_directory_love\",\n",
    "             workflow_type=\"ComputeMaterialGradients\",\n",
    "             salvus_flow_site_name=SALVUS_FLOW_SITE,\n",
    "             config=workflow_config,\n",
    "             use_celery=False)\n",
    "\n",
    "w.run()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gradient_love = UnstructuredMesh.from_h5(\"workflow_run_directory_love/gradient.h5\")\n",
    "gradient_love"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Or, for something more familiar, we can compute the sensitivty to transversely isotropic velocities."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "workflow_config[\"material_gradients\"].update(\n",
    "    get_gradient_settings(\"tti\")\n",
    ")\n",
    "\n",
    "# Remove the output directory if it already exists.\n",
    "if os.path.exists(\"workflow_run_directory_tti\"):\n",
    "    shutil.rmtree(\"workflow_run_directory_tti\")\n",
    "\n",
    "# Run our FWI workflow!!\n",
    "w = Workflow(base_dir=\"workflow_run_directory_tti\",\n",
    "             workflow_type=\"ComputeMaterialGradients\",\n",
    "             salvus_flow_site_name=SALVUS_FLOW_SITE,\n",
    "             config=workflow_config,\n",
    "             use_celery=False)\n",
    "\n",
    "w.run()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gradient_tti = UnstructuredMesh.from_h5(\"workflow_run_directory_tti/gradient.h5\")\n",
    "gradient_tti"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
