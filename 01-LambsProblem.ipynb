{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style='background-image: url(\"header.png\") ; padding: 0px ; background-size: cover ; border-radius: 5px ; height: 250px'>\n",
    "    <div style=\"float: right ; margin: 50px ; padding: 20px ; background: rgba(255 , 255 , 255 , 0.7) ; width: 50% ; height: 150px\">\n",
    "        <div style=\"position: relative ; top: 50% ; transform: translatey(-50%)\">\n",
    "            <div style=\"font-size: xx-large ; font-weight: 900 ; color: rgba(0 , 0 , 0 , 0.8) ; line-height: 100%\">Salvus</div>\n",
    "            <div style=\"font-size: large ; padding-top: 20px ; color: rgba(0 , 0 , 0 , 0.5)\">Lamb's Problem</div>\n",
    "        </div>\n",
    "    </div>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n",
    "An accurate solution to the wave equation is a requirement for a wide variety of seismological research. In this tutorial, we will validate the accuracy of Salvus_Compute_ by comparing numerically calculated seismograms to semi-analytical solutions of Lamb's Problem in 2-D. In addition to giving us confidence in the synthetic data we will use in future tutorials, it also gives us a chance to gently learn some of the key features of the Salvus API.\n",
    "\n",
    "Lamb's problem is concerned with the behavior of the elastic wave equation in the presence of a half-space bounded by a free-surface condition. In our solution we expect both direct arrivals and those reflected from the free-surface, along with a contribution from the 2-D Rayleigh wave. To validate the solutions generated with Salvus, we will compare our results with semi-analytical ones computed using [EX2DDIR](http://www.spice-rtn.org/library/software/EX2DDIR/softwarerelease.2006-11-10.9258147216.html). We will consider a half-space bounded at $y=2000$, and excite wave using a Ricker source with a center frequency of 15 Hz. This setup keeps compute times very low, while also allowing for a fair amount of wavelengths to propagate within our domain.\n",
    "\n",
    "Let's get started."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Meshing the domain\n",
    "\n",
    "In this example we will be working with a simple rectangular grid. As a first step, let's import our tools."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Python libraries.\n",
    "import os\n",
    "import copy\n",
    "import toml\n",
    "import shutil\n",
    "import numpy as np\n",
    "\n",
    "from pathlib import Path\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Salvus.\n",
    "import salvus_mesh             # Salvus meshing toolbox.\n",
    "import salvus_flow.api         # Salvus workflow toolbox.\n",
    "from pyasdf import ASDFDataSet # ASDF library for visualizing seismograms."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With everything imported, we can use the Python API of Salvus_Mesh_ to generate our simulation domain. In the cell below, we set all of the relevant parameters which will coincide with those we will use later on to generate our analytical solution. Our domain is of course finite -- but don't worry, we will handle infinity later on."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Domain size (m).\n",
    "max_x = 2000.\n",
    "max_y = 1000.\n",
    "\n",
    "# Material properties.\n",
    "vp  = 3200.  # P-wave velocity in m/s.\n",
    "vs  = 1847.5 # S-wave velocity in m/s.\n",
    "rho = 2200.  # Density in kg / m^3."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we need to pass some basic parameters which define to which frequencies our simulations will be accurate. Spectral-element simulations work best with around 2 4th-order elements per wavelength. In a medium as simple as this, we can analytically calculate how long our maximum element edge-length can be using the classic formula $v=f\\lambda$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Mesh resolution.\n",
    "max_frequency = 25.\n",
    "elm_per_wavelength = 2\n",
    "min_wavelength = vs / max_frequency\n",
    "max_edge_length = min_wavelength / elm_per_wavelength\n",
    "nelem_x = np.ceil(max_x / max_edge_length).astype(int)\n",
    "nelem_y = np.ceil(max_y / max_edge_length).astype(int)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we have all the information we need to generate a mesh accurate to 30 Hz. In the cell below we will create a rectangular mesh, and attach homogeneous material parameters to the mesh nodes. We'll follow this up by just calling the resulting mesh object, which allows us to visualize what we have done here in the notebook. Since we're just using a homogeneous model, things will look a bit boring... don't worry, things will pick up soon. Take a bit of time to familiarize yourself with the notebook visualization interface -- you can change the color scale, parameter to visualize, and the thickness of the mesh. You can also check out some basic information in the `Mesh Info` tab. While a great tool for generating and debugging your model, the in-notebook visualization tools are by design limited in scope. The mesh files used by Salvus can be directly opened in `Paraview` and `Visit` as well, so for fully-featured 3-D visualization these tools should be used."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Generate mesh.\n",
    "mesh = salvus_mesh.Skeleton([\n",
    "    salvus_mesh.StructuredGrid2D.rectangle(\n",
    "        nelem_x=nelem_x, nelem_y=nelem_y,\n",
    "        max_x=max_x, max_y=max_y)]).get_unstructured_mesh()\n",
    "\n",
    "# Automatically detect the boundaries of the mesh -- \n",
    "# useful for adding boundary conditions.\n",
    "mesh.find_side_sets()\n",
    "\n",
    "# Convert model parameterization.\n",
    "mesh.tensorize(order=1)\n",
    "\n",
    "# Attach parameters to the mesh.\n",
    "for node in range(mesh.nodes_per_element):\n",
    "    points_y = mesh.get_element_centroid()[:, 1]\n",
    "    mesh.attach_field(f\"RHO_{node}\", np.ones(mesh.nelem) * rho)\n",
    "    mesh.attach_field(f\"VP_{node}\", np.ones(mesh.nelem) * vp)\n",
    "    mesh.attach_field(f\"VS_{node}\", np.ones(mesh.nelem) * vs)\n",
    "    \n",
    "# All elements in this simulation are elastic, so \"fluid\" == False.\n",
    "mesh.attach_field(\"fluid\", np.zeros(mesh.nelem))\n",
    "\n",
    "# Write the model as hdf5 file.\n",
    "# In addition, this will create an xdmf file that Paraview can open.\n",
    "mesh.write_h5_tensorized_model(\"mesh.h5\", overwrite=True)\n",
    "\n",
    "# Visualize.\n",
    "mesh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Looking good! Now, with the mesh generated we can go ahead and start running some simulations.\n",
    "\n",
    "## Running simulations\n",
    "\n",
    "In this section we'll use the Salvus_Flow_ toolbox to start running some forward wavefield simulations. Salvus_Flow_ is meant to simplify the running of seismic simulations in today's heterogeneous compute environments. Whether you're running things locally, or remotely on a supercomputer, the interface stays the same. Salvus_Flow_ will take care of generating input files, copying files to and from remote compute clusters, and navigating any remote queueing systems. In later tutorials we'll use _Flow_ to run a true full-waveform inversion workflow, but in this tutorial we'll just use it to assist us in forward modelling.\n",
    "\n",
    "Let's just try and use _Flow_ to run _Compute_ out of the box. We'll pass 4 parameters:\n",
    "* `site_name`: This is an identifier which tells _Flow_ whether you're running on your local machine, some remote cluster, or perhaps the old chess computer in your grandfather's basement\n",
    "* `input_file`: This is a dictionary which acts as an input file to Salvus_Compute_. Note for the first run below we'll pass an empty dictionary to see how _Flow_ helps us decode invalid input files.\n",
    "* `ranks`: This is the number of MPI ranks the job will run on, and can range from 1 to whatever you'd like. All domain decomposition is handled at runtime.\n",
    "* `quiet`: By default, _Flow_ will output plently of status updates. This can get annoying in the notebooks however, so we'll turn it off here.\n",
    "\n",
    "Ok -- let's try and see what happens when we run _Compute_ with no inputs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "salvus_flow.api.run(\n",
    "    site_name=\"local\", output_folder=\"out\",\n",
    "    input_file={}, ranks=2, quiet=True, overwrite=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As expected, the simulation failed. Salvus_Flow_, Salvus_Compute_, and Salvus_Opt_ all provide a JSON schema which defines the proper format for the input file, the required properties, and in many cases an accepted range of values. The schemas of each of the sub-packages are read on-the-fly by Salvus_Flow_, and the provided input file is compared to what is expected. If the files do not match, then the job will fail immediately. While this can be a trivial advantage for small jobs, it really comes in handy with large simulations. I'm sure we've all been in the situation where we're waiting in a queue for hours only to have a job fail because of a typo in the input file! There are of course an infinite amount of ways to incorrectly specify your simulation parameters, and we can't catch all of them, but we hope that this setup goes a long way towards making dependent research more efficient.\n",
    "\n",
    "Extensive documentation on the input parameters can be found on [our website](https://mondaic.com/docs/index.html), where you can inspect all the groups of options which may be relevant. To save time, you can find a minimal example below. Let's start by adding the `domain` group that _Flow_ was complaining about above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_domain():\n",
    "    \"\"\"\n",
    "    A function which makes it easy to generate the 'domain' \n",
    "    group of a Salvus input file.\n",
    "    \"\"\"\n",
    "    return {\n",
    "        \"dimension\": 2,\n",
    "        \"polynomial-order\": 4,\n",
    "        \"mesh\": {\n",
    "            \"filename\": \"mesh.h5\",\n",
    "            \"format\": \"hdf5\"\n",
    "        },\n",
    "        \"model\": {\n",
    "            \"filename\": \"mesh.h5\",\n",
    "            \"format\": \"hdf5\"\n",
    "        }\n",
    "    }\n",
    "\n",
    "def get_hardware():\n",
    "    \"\"\"\n",
    "    A function which makes it easy to generate the 'hardware' \n",
    "    group of a Salvus input file.\n",
    "    \"\"\"\n",
    "    return {\n",
    "        \"gpu\": False\n",
    "    }\n",
    "\n",
    "\n",
    "# Generate the master input file and run.\n",
    "input_file = {\"domain\": get_domain(), \"hardware\": get_hardware()}\n",
    "salvus_flow.api.run(\n",
    "    site_name=\"local\", output_folder=\"out\",\n",
    "    input_file=input_file, ranks=2, overwrite=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Great! We've got an error message, but at least it looks like we've made some progress. Now _Flow_ is complaining that we didn't pass the `physics` group. Again, you can check out the documentation on the website to see how this should look, but to save time a relevant example is given below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_physics():\n",
    "    \"\"\"\n",
    "    A function which makes it easy to generate the 'domain' \n",
    "    group of a Salvus input file.\n",
    "    \"\"\"\n",
    "    return {\n",
    "        \"wave-equation\": {\n",
    "        \"time-stepping-scheme\": \"newmark\",\n",
    "        \"start-time-in-seconds\": -0.08,\n",
    "        \"end-time-in-seconds\": 0.52,\n",
    "        \"time-step-in-seconds\": 0.001,\n",
    "        \"attenuation\": False\n",
    "        }\n",
    "    }\n",
    "\n",
    "# Generate the master input file.\n",
    "input_file = {\"domain\": get_domain(), \n",
    "              \"hardware\": get_hardware(), \n",
    "              \"physics\": get_physics()}\n",
    "\n",
    "with open('run.toml', \"wt\") as fh:\n",
    "    toml.dump(input_file, fh)\n",
    "\n",
    "# Run Salvus.\n",
    "salvus_flow.api.run(\n",
    "    site_name=\"local\", output_folder=\"out\",\n",
    "    input_file=input_file, ranks=2, overwrite=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Alright. Now we can see that we've gotten a bit further. _Flow_ is no longer complaining about the structure of our input file, but is instead telling us that the source we have specified (`source.toml`) is not defined anywhere. I've taken the liberty of creating a source definition that matches with what we will eventually use to validate Lamb's problem. Let's write this and get on with our simulations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "source = {\n",
    "    \"name\": \"source1\",\n",
    "    \"location\": [1000.0, 500.0],\n",
    "    \"spatial-type\": \"vector\",\n",
    "    \"spatial-weights\": [0.0, -1e10],\n",
    "    \"source-time-function\" : {\n",
    "        \"type\": \"ricker\",\n",
    "        \"center-frequency\": 14.5\n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As a final step, let's also tell Salvus_Compute_ to generate a movie of the simulated wavefield. In addition to letting us validate that we're doing the right thing, I always find these wavefield movies quite relaxing."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# A minimal set of parameters specifying volumetric wavefield output.\n",
    "def produce_movie(fields=[\"displacement\"]):\n",
    "    \"\"\"\n",
    "    A function which encapsulates the parameters needed to \n",
    "    generate a movie of the wavefield.\n",
    "    \"\"\"\n",
    "    return {\n",
    "        \"volume-data\": {\n",
    "            \"fields\": fields,\n",
    "            \"sampling-interval-in-time-steps\": 1,\n",
    "            \"filename\": \"movie.h5\",\n",
    "            \"format\": \"hdf5\",\n",
    "            \"polynomial-order\": 4,\n",
    "            \"region-of-interest\": False\n",
    "        }\n",
    "    }\n",
    "\n",
    "physics = get_physics()\n",
    "physics[\"wave-equation\"][\"point-source\"] = [source]\n",
    "\n",
    "# Construct master input file.\n",
    "input_file = {\n",
    "    \"domain\": get_domain(),\n",
    "    \"hardware\": get_hardware(),\n",
    "    \"physics\": physics,\n",
    "    \"output\": produce_movie()\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, finally, we're ready to go. Below we'll run the simulation, wait for it to complete, collect its output (movie included), and then delete any temporary files. As mentioned above, we're running here on the `\"local\"` site due to the constraints of the tutorial, but the interface would look the same if we were running of some remote cluster. _Flow_ would log in over `ssh`, submit the job, and collect all the outputs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run a job.\n",
    "salvus_flow.api.run(\n",
    "    site_name=\"local\", output_folder=\"out\",\n",
    "    input_file=input_file, ranks=2, get_all=True,\n",
    "    overwrite=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The movie is now created -- if you look in this example directory you should see a `wavefield.h5`, or whatever you decided to name it (the .h5 suffix tells us that this, like the receivers, is also an hdf5 file). Additionally the corresponding XDMF file should be there. Feel free to download it to your local machine and open it with `Paraview`. In-notebook visualization is a feature which is coming quite soon, but it's currently in the beta-stage. Watch here for that to change."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Matching Lamb's problem"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "While visualizing the seismic wavefield is pretty cool, we of course came here to do something a bit more serious. In this section we'll add some point receivers to our mesh, and see whether or not we can re-create the Lamb's problem seismograms. Before we get started, let's see what the seismograms should look like by plotting the analytic solutions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_asdf_seismograms(asdf_file, axes):\n",
    "    \"\"\"\n",
    "    A simple utility function to plot multiple traces side-by side.\n",
    "    :param asdf_file: ASDF datafile containing receivers.\n",
    "    :param axes: Axes on which to plot.\n",
    "    \"\"\"    \n",
    "    for i, waveform in enumerate(asdf_file.waveforms):\n",
    "        for j, cmp in enumerate([\"X\", \"Y\"]):\n",
    "            trace = waveform.displacement.select(component=cmp)[0]\n",
    "            axes[j, i].plot(trace.data)\n",
    "            \n",
    "    # Add axis labels.\n",
    "    for a in axes[:, 0]:\n",
    "        a.set_ylabel(\"Displacement (m)\")\n",
    "    for a in axes[1, :]:\n",
    "        a.set_xlabel(\"Time sample\")\n",
    "            \n",
    "fig, axes = plt.subplots(2, 5, sharex='col', sharey='row', figsize=(20, 5))\n",
    "with ASDFDataSet(\"force_verticale.h5\", mode=\"r\") as dataset:\n",
    "    plot_asdf_seismograms(dataset, axes)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Definitely look like seismograms. Now, let's set up the same receivers in Salvus. The Lamb's problem solution was generated using 5 receivers located 200 m below the free surface and spaced in 100 m increments along the x-direction, with the first receiver offset to the right of the source by 10 m. Below we'll generate these as a `Python` list, and declare them as `point-data` output."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Parameters governing receiver placement.\n",
    "dx = 100\n",
    "x_start = 1010\n",
    "y_start = 800\n",
    "num_receivers = 5\n",
    "\n",
    "# Generate a list of all the receivers.\n",
    "receivers = [{\n",
    "    \"network-code\": \"XX\",\n",
    "    \"station-code\": f\"{_i:03d}\",\n",
    "    \"medium\": \"solid\",\n",
    "    \"location\": [float(x), float(y_start)]}\n",
    "    for _i, x in enumerate(np.arange(num_receivers) * dx + x_start)]\n",
    "\n",
    "def get_receivers(receivers):\n",
    "    return {\n",
    "        \"point-data\": {\n",
    "            \"fields\": [\"displacement\"],\n",
    "            \"sampling-interval-in-time-steps\": 1,\n",
    "            \"filename\": \"receivers.h5\",\n",
    "            \"format\": \"asdf\",\n",
    "            \"receiver\": receivers\n",
    "        }\n",
    "    }"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that we have our receivers defined, let's run a forward simulation and see how close we are to the semi-analytic solutions supplied by `EX2DDIR`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Construct master input file.\n",
    "input_file = {\n",
    "    \"hardware\": get_hardware(),\n",
    "    \"domain\": get_domain(),\n",
    "    \"physics\": physics,\n",
    "    \"output\": get_receivers(receivers),\n",
    "}\n",
    "\n",
    "\n",
    "with open('run.toml', \"wt\") as fh:\n",
    "    toml.dump(input_file, fh)\n",
    "# Run a job.\n",
    "salvus_flow.api.run(\n",
    "    site_name=\"local\", output_folder=\"out\",\n",
    "    input_file=input_file, ranks=2, get_all=True,\n",
    "    overwrite=True)\n",
    "\n",
    "\n",
    "# Plot results.\n",
    "fig, axes = plt.subplots(2, 5, sharex='col', sharey='row', figsize=(20,5))\n",
    "with ASDFDataSet('out/receivers.h5', mode=\"r\") as salvus,\\\n",
    "    ASDFDataSet('force_verticale.h5', mode=\"r\") as analytic:\n",
    "    plot_asdf_seismograms(analytic, axes)\n",
    "    plot_asdf_seismograms(salvus, axes)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Hmm. While we're close, there's something not quite right here. Remember when I mentioned above that we would make the domain infinite later on? Well, now is the time -- the discrepencies at later time are due to reflections off the free-surfaces of the domain. We'll now make our domain \"infinite\" by adding some absorbing boundaries at the edge of the domain. In this example we will add a simple Clayton-Enquist style boundary condition. While these boundaries do not perform well for waves hitting the boundaries at small angles, they are advantageous in that they do not need any sort of \"sponge-layers\". We will see how to attach more advanced absorbing boundaries in a future tutorial.\n",
    "\n",
    "For now, we will just update our `physics.wave-equation` group with some absorbing boundaries along the \"x0\", \"x1\", and \"y0\" side-sets."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Construct master input file.\n",
    "input_file = {\n",
    "    \"hardware\": get_hardware(),\n",
    "    \"domain\": get_domain(),\n",
    "    \"physics\": physics,\n",
    "    \"output\": get_receivers(receivers),\n",
    "}\n",
    "\n",
    "# Add absorbing boundaries to `wave-equation` group.\n",
    "input_file[\"physics\"][\"wave-equation\"].update({\n",
    "    \"boundaries\": [\n",
    "        {\n",
    "            \"type\": \"absorbing\",\n",
    "            \"side-sets\": [\"x0\", \"x1\", \"y0\"],\n",
    "            \"taper-amplitude\": 0.0,\n",
    "            \"width-in-meters\": 0.0\n",
    "        }\n",
    "    ]\n",
    "})\n",
    "\n",
    "# Run a job.\n",
    "salvus_flow.api.run(\n",
    "    site_name=\"local\", output_folder=\"out\",\n",
    "    input_file=input_file, ranks=2, get_all=True,\n",
    "    overwrite=True)\n",
    "\n",
    "# Plot results.\n",
    "fig, axes = plt.subplots(2, 5, sharex='col', sharey='row', figsize=(20,5))\n",
    "with ASDFDataSet('out/receivers.h5', mode=\"r\") as salvus,\\\n",
    "    ASDFDataSet('force_verticale.h5', mode=\"r\") as analytic:\n",
    "    plot_asdf_seismograms(analytic, axes)\n",
    "    plot_asdf_seismograms(salvus, axes)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Looks like that did it :).\n",
    "\n",
    "While this is a neat way to introduce some of the simple forward modelling commands, this tutorial has a deeper significance to the Salvus project as a whole. In fact, this very notebook, along with many like it, get run every time a new commit is made to the Salvus codebase. If the solutions do not fit as well as is visualized above, the commit is rejected. This helps us ensure that, as more and more features are added, our solutions remain accurate and performant."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Under the hood\n",
    "\n",
    "While Salvus_Flow_ is a great tool for running simulations both large and small, sometimes it doesn't make sense to deal with the overhead of firing up a `Python` notebook. Fortunatelly, it's quite easy to run Salvus_Compute_ from the command line as well -- all we need to do is generate a `toml` input file. Let's generate a file from the inputs we've been using so far."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Generate the complete input file.\n",
    "input_file = {\n",
    "    \"hardware\": get_hardware(),\n",
    "    \"domain\": get_domain(),\n",
    "    \"physics\": physics,\n",
    "    \"output\": get_receivers(receivers),\n",
    "}\n",
    "\n",
    "# Write the input file as a toml.\n",
    "with open(\"sample_toml_file.toml\", \"w\") as fh:\n",
    "    toml.dump(input_file, fh)\n",
    "    \n",
    "# Look at the file we've written.\n",
    "!cat \"sample_toml_file.toml\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With this input file you can now just run Salvus_Compute_ from the command line, with or without MPI. You can also of course modify the input files, as long as they are consistent with the schema. As with Salvus_Flow_, most invalid inputs will trigger an informative schema error before the time loop begins.\n",
    "\n",
    "That's it for this tutorial!"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
